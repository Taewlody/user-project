import * as React from 'react';
import { Card, Form, Button, Col, Row, Modal} from 'react-bootstrap';
import userImg from './image/user.jpg';
import { Link } from 'react-router-dom';

const pattern_text_th = /[ก-๙]+$/;
const pattern_text = /^[a-zA-Z ]+$/;
const pattern_username = /^[A-Za-z0-9_.]+$/;
class Login extends React.Component {
    state = {
        isModal: false,
        username: '',
        password: '',
        firstname: '',
        lastname: '',
        profile_img: '',
    };

    setShowModal = () => {
        this.setState({ 
            isModal: true,
            username: '',
            password: '',
            firstname: '',
            lastname: '',
            profile_img: '', 
        });
    };

    register = (e) => {
        
    }

    setValue = (e, name) => {
        if(name == 'firstname' || name == 'lastname'){
            if (pattern_text.test(e.target.value) || pattern_text_th.test(e.target.value)){
                this.setState({
                    [name]: e.target.value
                });               
                
            }else{
                e.preventDefault();
                
            }

        }else if(name == "username") {
            if (!pattern_username.test(e.target.value)){
                e.preventDefault();
                
            }else{
                this.setState({
                    [name]: e.target.value
                });
            }
        }else{
            this.setState({
                [name]: e.target.value
            });
        }
    }

    render() {
        return(
            <>
                <Modal backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                    <Modal.Header closeButton>
                        <Modal.Title as="h5">Register</Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="f-12">
                        <Row>
                            <Col sm={4}>
                                <Form.Label>Username: </Form.Label>
                            </Col>
                            <Col sm={8}>
                                <Form.Control type="text" value={this.state.username} onChange={e => this.setValue(e, "username")}/>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4}>
                                <Form.Label>Password: </Form.Label>
                            </Col>
                            <Col sm={8}>
                                <Form.Control type="text" value={this.state.password} onChange={e => this.setValue(e, "password")}/>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4}>
                                <Form.Label>Firstname: </Form.Label>
                            </Col>
                            <Col sm={8}>
                                <Form.Control type="text" value={this.state.firstname} onChange={e => this.setValue(e, "firstname")}/>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4}>
                                <Form.Label>Lastname: </Form.Label>
                            </Col>
                            <Col sm={8}>
                                <Form.Control type="text" value={this.state.lastname} onChange={e => this.setValue(e, "lastname")}/>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4}>
                                <Form.Label>Profile Image: </Form.Label>
                            </Col>
                            <Col sm={8}>
                                <Form.Control type="file" value={this.state.profile_img} onChange={e => this.setValue(e, "profile_img")}/>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                        <Button variant="primary" onClick={e => this.register(e)}>Submit</Button>
                    </Modal.Footer>
                </Modal>

                <Card style={{margin: "10% 20% 0 20%", minHeight: "350px"}}>
                    <Card.Body>
                        <Form.Group as={Row}>
                            <Col sm={6} style={{borderRight: '1px solid lightgrey'}}>
                                <Form.Group as={Row}>
                                   <img src={userImg} style={{borderRadius: '50%'}}/>
                                </Form.Group>
                                
                            </Col>
                            <Col sm={1}></Col>
                            <Col sm={4} style={{paddingTop: '3%'}}>
                                <Form.Group as={Row}>
                                    <Form>
                                        <Form.Group className="text-center">
                                            <Form.Label> LOGIN</Form.Label>
                                        </Form.Group>

                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Username</Form.Label>
                                            <Form.Control type="username" placeholder="Enter Username" />
                                        </Form.Group>

                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control type="password" placeholder="Enter Password" />
                                        </Form.Group>
                                        <Form.Group className="text-center">
                                            <Link to={"/edit"}>
                                            <Button className="text-center" variant="success" type="submit">
                                                Sign In
                                            </Button>
                                            </Link> &nbsp;
                                            <Button className="text-center" variant="primary" type="button" onClick={e => this.setShowModal()}>
                                                Sign Up
                                            </Button>
                                        </Form.Group>
                                    </Form> 
                                </Form.Group>
                            </Col>
                        </Form.Group>
                    </Card.Body>
                </Card>
                    
                
            </>
        )
    }

}

export default Login;