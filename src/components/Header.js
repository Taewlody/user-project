import * as React from 'react';
import logo from './image/group.png';
import styled from 'styled-components';
import { Form, Col, Row} from 'react-bootstrap';

const HeaderBar = styled.header`
    width: 100%;
    padding: 0.5em 1em;
    display: flex;
    height: 64px;
    position: fixed;
    align-items: center;
    background-color: #6157F1;
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.25);
    z-index: 1;
`;

const Header = () => {
    return (
        <HeaderBar>
            <Row className="w-100">
                <Col sm={12}>
                    <Form.Group as={Row}>
                        <Col sm={6}>
                            <img src={logo} alt="User System Logo" height="50" style={{borderRadius: 50, backgroundColor: 'white'}} />
                            <Form.Label style={{color: 'white', marginLeft: '2%'}}>User System</Form.Label>
                        </Col>
                    </Form.Group>
                </Col>
            </Row>
        </HeaderBar>
    )
}

export default Header;