import * as React from 'react';
import { Card } from 'react-bootstrap';
import Layout from './Layout'
import { Form, Col, Row, Button} from 'react-bootstrap';
import './style.css';
import user from './image/user2.png'
import { faUserCog } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const pattern_text_th = /[ก-๙]+$/;
const pattern_text = /^[a-zA-Z ]+$/;
const pattern_username = /^[A-Za-z0-9_.]+$/;
class EditProfile extends React.Component {
    state = {
        isModal: false,
        username: '',
        password: '',
        firstname: '',
        lastname: '',
        profile_img: '',
        
    };

    handleSubmit = (event) => {
        fetch('/user_log')
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            if(this.state.password){
                if(this.state.password == data.password){
                    alert('ไม่สามารถใช้รหัสซ้ำกับที่เคยใช้ได้');
                }
            }else{
                alert('บันทึกสำเร็จ');
            }
        });

        event.preventDefault();
    }

    setValue = (e, name) => {
        if(name == 'firstname' || name == 'lastname'){
            if (pattern_text.test(e.target.value) || pattern_text_th.test(e.target.value)){
                this.setState({
                    [name]: e.target.value
                });               
                
            }else{
                e.preventDefault();
                
            }

        }else if(name == "username") {
            if (!pattern_username.test(e.target.value)){
                e.preventDefault();
                
            }else{
                this.setState({
                    [name]: e.target.value
                });
            }
        }else{
            this.setState({
                [name]: e.target.value
            });
        }
    }

    getDataUser = () => {
        fetch('/user')
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            this.setState({
                username: data.username,
                firstname: data.firstname,
                lastname: data.lastname,
            });
        });
    }

    componentDidMount() {
        this.getDataUser();
    }

    render() {
        return(
            <>
                <Layout>
                <Form onSubmit={this.handleSubmit}>
                    <Card style={{margin: "0% 5% 0 5%"}}>
                        <Card.Header>
                            {/* <i class="fas fa-user-cog"></i> */}
                            <FontAwesomeIcon icon={faUserCog} /> &emsp;
                            <Form.Label>
                                Edit Profile
                            </Form.Label>
                        </Card.Header>
                        <Card.Body>
                        <Row>
                            <Col sm={12} className="text-center">
                                <img src={user} height="200" style={{borderRadius: 50, backgroundColor: 'white'}} />
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={12} className="text-center">
                                <Button className="btn-primary">Change Picture</Button>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4} className="text-right">
                                <Form.Label>Username: </Form.Label>
                            </Col>
                            <Col sm={5}>
                                <Form.Control type="text" value={this.state.username} onChange={e => this.setValue(e, "username")} required/>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4} className="text-right">
                                <Form.Label>Password: </Form.Label>
                            </Col>
                            <Col sm={5}>
                                <Form.Control type="text" value={this.state.password} onChange={e => this.setValue(e, "password")}/>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4} className="text-right">
                                <Form.Label className="text-right">Firstname: </Form.Label>
                            </Col>
                            <Col sm={5}>
                                <Form.Control type="text" value={this.state.firstname} onChange={e => this.setValue(e, "firstname")} required/>
                            </Col>
                        </Row><br/>
                        <Row>
                            <Col sm={4} className="text-right">
                                <Form.Label className="text-right">Lastname: </Form.Label>
                            </Col>
                            <Col sm={5}>
                                <Form.Control type="text" value={this.state.lastname} onChange={e => this.setValue(e, "lastname")} required/>
                            </Col>
                        </Row>
                        </Card.Body>

                        <Card.Footer>
                            <Row>
                                <Col sm={10}></Col>
                                <Col sm={2} className="text-right">
                                    <Button className="btn-success" type="submit">Save</Button>
                                </Col>
                            </Row>
                        </Card.Footer>
                    </Card>
                </Form>
                </Layout>
            </>
        )
    }
}

export default EditProfile;