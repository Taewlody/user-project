import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import styled from 'styled-components';
import user from './image/user2.png';
import { faUserCog } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Nav = styled.nav`
    padding: 1em;
    // background: #f7f3e9;
    @media (max-width: 700px) {
        padding-top: 64px;
    }
    @media (min-width: 700px) {
        position: fixed;
        width: 220px;
        height: calc(100% - 64px);
    }
`;
const NavList = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
    line-height: 2;
    a {
        text-decoration: none;
        font-weight: bold;
        font-size: 1em;
        color: #333;
    }
    a:visited {
        color: #333;
    }
    a:hover,
    a:focus {
        color: #0077cc;
    }
`;

const Navigation = () => {
    return (
        <Nav>
            <NavList>
                <li className="text-center">
                    <img src={user} height="50" style={{borderRadius: 50, backgroundColor: 'white'}} />
                </li>
                <li className="text-center">
                    Username
                </li>
                <li className="text-center">
                    <Link to="/">
                        Log out
                    </Link>
                </li>
                <hr/>
                {/* <li activeStyle={{color: "red"}}> */}
                    <NavLink to="/edit">
                        <FontAwesomeIcon icon={faUserCog} /> &nbsp; Profile
                    </NavLink>
                {/* </li> */}
                
            </NavList>
        </Nav>
    );
};
export default Navigation;