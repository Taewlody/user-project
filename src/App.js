import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import EditProfile from './components/EditProfile';
import background from './background.jpg'
import 'bootstrap/dist/css/bootstrap.css';
import { Card, Form, Button, Col, Row} from 'react-bootstrap';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

function App() {
  // state = {
  //   data: null
  // };

  // componentDidMount() {
  //   this.callBackendAPI()
  //     .then(res => this.setState({ data: res.express }))
  //     .catch(err => console.log(err));
  // }
  //   // fetching the GET route from the Express server which matches the GET route from server.js
  // callBackendAPI = async () => {
  //   const response = await fetch('/express_backend');
  //   const body = await response.json();

  //   if (response.status !== 200) {
  //     throw Error(body.message) 
  //   }
  //   return body;
  // };

  return (
    // <div style={{ backgroundImage: "url('/background.jpg')" }}>
    <Router>
      {/* <div className="App-header"> */}
        {/* <Login /> */}
        <Switch>
        <Route path={"/"} exact component={Login}/>
        <Route path={"/edit"} component={EditProfile}/>
        </Switch>
        
      {/* </div> */}
    </Router>
  );
}

const Home = () => (
  <div>
    <h1>home</h1>
  </div>
)

export default App;
