const express = require('express'); //Line 1
const app = express(); //Line 2
const port = process.env.PORT || 3000; //Line 3

// This displays message that the server running and listening to specified port
app.listen(port, () => console.log(`Listening on port ${port}`)); //Line 6

// create a GET route
app.get('/express_backend', (req, res) => { //Line 9
  res.send({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT' }); //Line 10
}); //Line 11

app.get('/user', (req, res) => {
    res.send({
        id: 1,
        firstname: 'Admin',
        lastname: 'Admin',
        username: 'Admin',
        password: '123456'
    })
})

app.get('/user_log', (req, res) => {
    // res.send({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT' });
    // res.json(
    //     {
    //         id: 1,
    //         firstname: 'number 1',
    //         lastname: 'number 1',
    //         username: 'number 1',
    //         password: '123456',
    //         update_by: '1',
    //         updated_at: '2021-10-01',
    //     },
    //     {
    //         id: 1,
    //         firstname: 'number 2',
    //         lastname: 'number 2',
    //         username: 'number 2',
    //         password: '123456',
    //         update_by: '1',
    //         updated_at: '2021-10-02',
    //     },
    //     {
    //         id: 1,
    //         firstname: 'number 3',
    //         lastname: 'number 3',
    //         username: 'number 3',
    //         password: '123456',
    //         update_by: '1',
    //         updated_at: '2021-10-03',
    //     },
    //     {
    //         id: 1,
    //         firstname: 'number 4',
    //         lastname: 'number 4',
    //         username: 'number 4',
    //         password: '123456',
    //         update_by: '1',
    //         updated_at: '2021-10-04',
    //     },
    //     {
    //         id: 1,
    //         firstname: 'number 5',
    //         lastname: 'number 5',
    //         username: 'number 5',
    //         password: '123456',
    //         update_by: '1',
    //         updated_at: '2021-10-05',
    //     },
    //     {
    //         id: 1,
    //         firstname: 'number 6',
    //         lastname: 'number 6',
    //         username: 'number 6',
    //         password: '123456',
    //         update_by: '1',
    //         updated_at: '2021-10-06',
    //     }
    // )
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
        id: 1,
        firstname: 'number 1',
        lastname: 'number 1',
        username: 'number 1',
        password: '123456',
        update_by: '1',
        updated_at: '2021-10-01',
    },
    {
        id: 1,
        firstname: 'number 2',
        lastname: 'number 2',
        username: 'number 2',
        password: '123456',
        update_by: '1',
        updated_at: '2021-10-02',
    },
    {
        id: 1,
        firstname: 'number 3',
        lastname: 'number 3',
        username: 'number 3',
        password: '123456',
        update_by: '1',
        updated_at: '2021-10-03',
    },
    {
        id: 1,
        firstname: 'number 4',
        lastname: 'number 4',
        username: 'number 4',
        password: '123456',
        update_by: '1',
        updated_at: '2021-10-04',
    },
    {
        id: 1,
        firstname: 'number 5',
        lastname: 'number 5',
        username: 'number 5',
        password: '123456',
        update_by: '1',
        updated_at: '2021-10-05',
    },
    {
        id: 1,
        firstname: 'number 6',
        lastname: 'number 6',
        username: 'number 6',
        password: '123456',
        update_by: '1',
        updated_at: '2021-10-06',
    }
    ));
})